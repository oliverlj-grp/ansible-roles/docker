# CHANGELOG

<!--- next entry here -->

## 0.1.0
2020-06-14

### Features

- initial commit (253faaae632204b1b5d363af3462d603a4077fc0)

### Fixes

- add mit license (76d043561e9c625ba1841f7b6ddecb3a7b35e322)