# Ansible role docker

Install docker on ubuntu 20.04

## Requirements

Assumes ubuntu 20.04

## Things done

```plantuml
@startuml
hide empty description
[*] --> docker_apt
docker_apt --> docker_install
docker_install --> docker_no_upgrade
docker_no_upgrade --> docker_cleanup
docker_cleanup --> [*]

state "Add docker apt repository" as docker_apt
state "Install docker-ce apt package" as docker_install
state "Disable docker-ce from unattended upgrades" as docker_no_upgrade
state "Add cleanup cron job of containers, networks and images" as docker_cleanup

@enduml
```

## Role Variables

None

## Example Playbook

```yaml
- hosts: all
  roles:
    - docker
```

## License

MIT
